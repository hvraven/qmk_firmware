# Custom layout for redox

Based on the german layout, but with optional layer switch to a neo 2
based layout

The layout acommodates the German umlauts and the punctuation symbols
at their usual positions relative to the other alphabetic characters.
Apart from that it stays close to the default english redox layout. 
